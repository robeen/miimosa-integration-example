'use strict';

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    bulkSass = require('gulp-sass-bulk-import'),
    watch = require('gulp-watch');

var sassBuild = function(){
    gulp.src( './assets/css/style.scss' )
        .pipe( bulkSass() )
        .pipe( sass().on('error', sass.logError) )
        .pipe( gulp.dest( '.' ));

    var currentdate = new Date(); 
    var datetime = currentdate.getDate() + "/"
        + (currentdate.getMonth()+1)  + "/" 
        + currentdate.getFullYear() + " @ "  
        + currentdate.getHours() + ":"  
        + currentdate.getMinutes() + ":" 
        + currentdate.getSeconds();
    console.log('Just compiled sass : ' + datetime);
};

gulp.task('sass', [], function() {
    sassBuild();
});

//Watch task
gulp.task('default',function() {
    return watch('./assets/css/**/*.scss', function () {
      sassBuild();
    });
});